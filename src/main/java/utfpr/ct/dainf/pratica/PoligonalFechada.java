/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1796283
 */
public class PoligonalFechada<T extends Ponto2D> extends Poligonal<T> {
    public double getComprimento() {
        if(getN() <= 1) return 0;
        else {
            return super.getComprimento() + get(getN()-1).dist(get(0));
        }
    }
}
