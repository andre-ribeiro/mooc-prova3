package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    private final List<T> vertices = new ArrayList<>();
    
    public int getN() {
        return vertices.size();
    }
    
    public T get(int i) {
        if(i >= getN()) throw new RuntimeException(String.format("get(%d): indice inválido", i));
        return vertices.get(i);
    }
    
    public void set(int i, T vertice) {
        if(i == getN()) vertices.add(vertice);
        else if(i < getN()) vertices.set(i, vertice);
        else throw new RuntimeException(String.format("set(%d): indice inválido", i));
    }
    
    public double getComprimento() {
        double tamanho = 0;
        if(getN() <= 1) return 0;
        else {
            for(int i = 0; i < getN()-1; i++) {
                tamanho += get(i).dist(get(i+1));
            }
        }
        return tamanho;
    }
}
